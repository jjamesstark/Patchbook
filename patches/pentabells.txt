LEAD:
  - Keystep37 (Pitch) p> SV1B (VCO1 Pitch)
  - Keystep37 (Mod) >> QuadVCA (Ch 4 In)
  - SV1B (LFO Triange) >> QuadVCA (Ch 4 CV)
  * SV1B: LFO = 75%
  - QuadVCA (Ch4 Out) >> SV1B (VCO1 FM)
  * SV1B: VCO1 FM = 10% (to taste)
  * QuadVCA: L4 = 25% | CV4 = 100%
  - SV1B (Noise) -> SV1B (Mix1 Ch 1)
  - SV1B (VCO1 Triangle) -> SV1B (Mix1 Ch 2)
  - SV1B (Mix1 Out) -> SincBucina (Audio In)
  - Keystep37 (Gate) g> SincBucina (Gate)
  - Keystep37 (Gate) g> Maths (Ch 1 Trigger)
  - Maths (Ch 1) >> SV1B (Filter Freq)
  * Maths:
  | Ch 1 Attn = 55% (LFO to SV1B filter)
  | Ch 1 Rise = 66%
  | Ch 1 Fall = 66%
  | Ch 1 Curve = Lin
  - SincBucina (Audio Out) -> SV1B (Filter In)
  * SincBucina: Attack = 50% | Release = 100% | Slope = 6
  - SV1B (Filter LP) -> SV1B (Mix2 Ch 1)
  - SV1B (Filter BP) -> SV1B (Mix2 Ch 2)
  * SV1B: 
  | Filter Freq = 75%
  | Filter Freq CV = 30% (to taste)
  | Filter Resonance = 90% (to taste)
  - SV1B (Mix2 Out) -> QuadVCA (Ch 3 In)
  - QuadVCA (Selected) -> amp (In)

BELLS:
  - Plaits (Out) -> Morphagene (Left In)
  * Plaits:
  | Model = Inharmonic String
  | Freq = 25%
  | Harm = 50%
  | Timbre = 50%
  | Timbre CV Attn = 75%
  | Morph = 50%
  - Maths (Ch 1) >> Plaits (Timbre)
  - Pamelas (Ch 5) g> Plaits (Trigger)
  * Pamelas:
  | Ch 5 Modifier = x2
  | Ch 5 Wave = Gate
  | Ch 5 rskip = 20%
  - Pamelas (Ch 6) p> Plaits (v/oct)
  * Pamelas:
  | Ch 6 Modifier = x2
  | Ch 6 Wave = Stepped Random
  | Ch 6 Level = 70%
  | Ch 6 Offset = 55%
  | Ch 6 Width = 5%
  | Ch 6 Quant = Pm
  - Maths (Ch 4) >> SV1B (VCA CV)
  * Maths:
  | Ch 2 Attn = 30%
  | Ch 3 Attn = 25%
  | Ch 4 Attn = 50%-100% (LFO attn noise to varispeed)
  | Ch 4 Rise = 45%
  | Ch 4 Fall = 45%
  | Ch 4 Curve = Lin
  - Maths (Sum) >> Maths (Ch 4 Both)
  - SV1B (Noise) -> SV1B (VCA In)
  - SV1B (VCA Out) -> Morphagene (varispeed)
  - Morphagene (Left Out) -> QuadVCA (Ch 1 In)
  - Pamelas (Ch 7) -> Morphagene (geneSize)
  * Pamelas: Ch 7 Modifier = /12 | Ch 7 Wave = Triange
  - Pamelas (Ch 8) -> Morphagene (morph)
  * Pamelas: Ch 8 Modifier = /11 | Ch 8 Wave = Smooth Random
  * Morphagene: Size = 50% | Size CV Attn = 0% | Morph = 50%

BASS:
  - Pamelas (Ch 2) g> QuadVCA (Ch 2 CV)
  - Pamelas (Ch 4) p> SV1B (VCO2 Pitch)
  * Pamelas:
  | Ch 2 Modifier = x1
  | Ch 2 Wave = Gate
  | Ch 2 Width = 100%
  | Ch 4 Modifier = /3
  | Ch 4 Wave = Stepped Random
  | Ch 4 Level = 33%
  | Ch 4 rskip = 25%
  | Ch 4 Quant = P5
  - SV1B (VCO2 Sine) -> QuadVCA (CH 2 In)

  * Pamelas:
  | BPM = 120

// Parameters for Play
//    Keystep37 pitch& Mod (LEAD)
//    Maths Channel 4 Attn (varispeed distortion level)
//    Lead sounds best playing pentatic scale
//    Start out with only lead or only base. 
//    Expirimenting with Mixer (QuadVCA) on balancing voices
